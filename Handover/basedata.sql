
# create database and use it
CREATE DATABASE IF NOT EXISTS handover;
USE handover;

# create the customer table
CREATE TABLE IF NOT EXISTS CUSTOMER(
id       int unique key not null auto_increment primary key,
address  varchar(255) null,
email    varchar(255) null,
password varchar(255) null,
role     varchar(255) null,
username varchar(255) null,
UNIQUE (username)
);

# insert default customers
INSERT INTO CUSTOMER(id, address, email, password, role, username)
VALUES (1, 'via Garibaldi 11', 'admin@test.it', '123', 'ROLE_ADMIN', 'admin');

# create the product table
CREATE TABLE IF NOT EXISTS PRODUCT(
id  int unique key not null auto_increment primary key,
description varchar(255) null,
image       varchar(255) null,
name        varchar(255) null,
price       int null,
quantity    int null,
customer_id int null
);

CREATE INDEX FKt23apo8r9s2hse1dkt95ig0w5
    ON PRODUCT (customer_id);