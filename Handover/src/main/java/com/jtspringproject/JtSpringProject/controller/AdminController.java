package com.jtspringproject.JtSpringProject.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import com.jtspringproject.JtSpringProject.models.Product;
import com.jtspringproject.JtSpringProject.models.User;
import com.jtspringproject.JtSpringProject.services.ProductService;
import com.jtspringproject.JtSpringProject.services.UserService;

@Controller
@RequestMapping("/admin")
public class AdminController {
	
	@Autowired
	private UserService userService;

	@Autowired
	private ProductService productService;
	
	int adminlogcheck = 0;
	String usernameforclass = "";

	@RequestMapping(value = {"/","/logout"})
	public String returnIndex() {
		adminlogcheck =0;
		usernameforclass = "";
		return "userLogin";
	}

	@GetMapping("/index")
	public String index(Model model) {
		if (usernameforclass.equalsIgnoreCase(""))
			return "userLogin";
		else {
			model.addAttribute("username", usernameforclass);
			return "index";
		}
	}

	@GetMapping("login")
	public String adminlogin() {
		return "adminlogin";
	}

	@GetMapping("Dashboard")
	public String adminHome(Model model) {
		if (adminlogcheck==1)
			return "adminHome";
		else
			return "redirect:/admin/login";
	}

	@GetMapping("/loginvalidate")
	public String adminlog(Model model) {
		return "adminlogin";
	}

	@RequestMapping(value = "loginvalidate", method = RequestMethod.POST)
	public ModelAndView adminlogin( @RequestParam("username") String username, @RequestParam("password") String pass) {

		User user=this.userService.checkLogin(username, pass);

		if (user.getRole() != null && user.getRole().equals("ROLE_ADMIN")) {
			ModelAndView mv = new ModelAndView("adminHome");
			adminlogcheck=1;
			mv.addObject("admin", user);
			return mv;
		} else {
			ModelAndView mv = new ModelAndView("adminlogin");
			mv.addObject("msg", "Please enter correct username and password");
			return mv;
		}
	}

	@GetMapping("products")
	public ModelAndView getproduct() {
		if (adminlogcheck==0){
			ModelAndView mView = new ModelAndView("adminlogin");
			return mView;
		} else {
			ModelAndView mView = new ModelAndView("products");
			List<Product> products = this.productService.getProducts();

			if (products.isEmpty()) {
				mView.addObject("msg", "No products are available");
			} else {
				mView.addObject("products", products);
			}
			return mView;
		}
	}

	@GetMapping("products/add")
	public ModelAndView addProduct() {
        return new ModelAndView("productsAdd");
	}

	@RequestMapping(value = "products/add",method=RequestMethod.POST)
	public String addProduct(@RequestParam("name") String name,@RequestParam("price") int price, @RequestParam("quantity") int quantity,@RequestParam("description") String description,@RequestParam("productImage") String productImage) {
		Product product = new Product();
		product.setName(name);
		product.setDescription(description);
		product.setPrice(price);
		product.setImage(productImage);
		product.setQuantity(quantity);
		this.productService.addProduct(product);
		return "redirect:/admin/products";
	}

	@GetMapping("customers")
	public ModelAndView getCustomerDetail() {
		if (adminlogcheck == 0){
			return new ModelAndView("adminlogin");
		} else {
			ModelAndView mView = new ModelAndView("displayCustomers");
			List<User> users = this.userService.getUsers();
			mView.addObject("customers", users);
			return mView;
		}
	}

}
