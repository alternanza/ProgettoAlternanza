import java.sql.*;
public class LibreriaService {
    public void getLibro(Statement statement, String url, String username, String password){
        try{
            //Stampa iniziale
            String queryTito = "Select * from libro";
            ResultSet resu = statement.executeQuery(queryTito);
            while (resu.next()) {
                int ID = resu.getInt("ID");
                String ISBN = resu.getString("ISBN");
                String Titolo = resu.getString("Titolo");
                String Lingua = resu.getString("Lingua");
                String Editore = resu.getString("Editore");
                int AnnoProd= resu.getInt("AnnoPubb");
                System.out.println(ID+", "+ISBN + ", " + Titolo +
                        ", " + Lingua + ", " + Editore+ ", " + AnnoProd+".");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void addLibro(Connection conn, Statement statement, String url, String username, String password, String ISBN1, String Titolo1, String Lingua1, String Editore1, int AnnoPubb1){
        try{
            String insertBook = "INSERT INTO libro (ISBN, Titolo, Lingua, Editore, AnnoPubb) VALUES (?, ?, ?, ?, ?)";
            PreparedStatement preparedStatement = conn.prepareStatement(insertBook);
            preparedStatement.setString(1, ISBN1);
            preparedStatement.setString(2, Titolo1);
            preparedStatement.setString(3, Lingua1);
            preparedStatement.setString(4, Editore1);
            preparedStatement.setInt(5, AnnoPubb1);
            int rowsInserted = preparedStatement.executeUpdate();
            System.out.println(rowsInserted + " Libro Aggiunto.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void modLibro(Connection conn, Statement statement, String url, String username, String password, int ID1, String ISBN1, String Titolo1, String Lingua1, String Editore1, int AnnoPubb1){
        try{
            String updateBook = "UPDATE libro SET ISBN = ?, Titolo=?, Lingua=?, Editore=?, AnnoPubb=? WHERE ID=?";
            PreparedStatement preparedStatement = conn.prepareStatement(updateBook);
            preparedStatement.setString(1, ISBN1);
            preparedStatement.setString(2, Titolo1);
            preparedStatement.setString(3, Lingua1);
            preparedStatement.setString(4, Editore1);
            preparedStatement.setInt(5, AnnoPubb1);
            preparedStatement.setInt(6, ID1);
            int rowsUpdated = preparedStatement.executeUpdate();
            System.out.println(rowsUpdated + " Libro Aggiornato");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void delLibro(Connection conn, Statement statement, String url, String username, String password, int ID1){
        try{
            String deleteBook = "DELETE FROM libro WHERE ID = (?)";
            PreparedStatement preparedStatement = conn.prepareStatement(deleteBook);
            preparedStatement.setInt(1, ID1);
            int rowsDeleted = preparedStatement.executeUpdate();
            System.out.println(rowsDeleted + " Libro Cancellato");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
