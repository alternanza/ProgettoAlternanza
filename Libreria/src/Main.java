import java.sql.*;
import java.util.Scanner;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    static LibreriaService service = new LibreriaService();
    static int rispo=0, ID;
    static Scanner sc=new Scanner(System.in);
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        String ISBN0, ISBN, Titolo, Editore, Lingua;
        int AnnoPubb;
        try {
        System.out.println("\nMi Connetto al DataBase");
        String url = "jdbc:mysql://localhost:3306/Libreria";
        String username = "root";
        String password= "mysql";
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection conn = DriverManager.getConnection(url,username,password);
        Statement statement = conn.createStatement();
        System.out.println("\nConnesione Riuscita!\n");
        System.out.println("\nEcco i Libri Presenti nel DB!\n");
        service.getLibro(statement, url, username, password);
        while(rispo!=4){
            System.out.println("\nChe Operazione vuoi fare?");
            System.out.println("1 -> Inserire un Libro Nuovo");
            System.out.println("2 -> Modificare un Libro");
            System.out.println("3 -> Eliminare un Libro");
            System.out.println("4 -> Terminare");
            rispo=sc.nextInt();
            switch (rispo){
                case 1:{
                    System.out.println("\nInseriamo un Nuovo Libro!\n");
                    System.out.println("Qual'è l'ISBN del nuovo Libro?");
                    ISBN=sc.next();
                    System.out.println("Qual'è il Titolo del nuovo Libro?");
                    Titolo=sc.next();
                    System.out.println("Qual'è la Lingua del nuovo Libro?");
                    Lingua=sc.next();
                    System.out.println("Qual'è l'Editore del nuovo Libro?");
                    Editore=sc.next();
                    System.out.println("Qual'è l'anno di pubblicazione del nuovo Libro?");
                    AnnoPubb=sc.nextInt();
                    service.addLibro(conn, statement, url, username, password, ISBN, Titolo, Lingua, Editore, AnnoPubb);
                }break;
                case 2:{
                    System.out.println("\nAggiorniamo un Libro Presente!\n");
                    System.out.println("Qual'è l'ID del Libro che desideri Aggiornare?");
                    ID=sc.nextInt();
                    System.out.println("Qual'è il nuovo ISBN?");
                    ISBN=sc.next();
                    System.out.println("Qual'è il nuovo Titolo?");
                    Titolo=sc.next();
                    System.out.println("Qual'è la nuova Lingua?");
                    Lingua=sc.next();
                    System.out.println("Qual'è il nuovo Editore?");
                    Editore=sc.next();
                    System.out.println("Qual'è il nuovo anno di pubblicazione?");
                    AnnoPubb=sc.nextInt();
                    service.modLibro(conn, statement, url, username, password, ID, ISBN, Titolo, Lingua, Editore, AnnoPubb);
                }break;
                case 3:{
                    System.out.println("\nEliminiamo un Libro Presente!");
                    System.out.println("Qual'è l'ID del Libro che desideri cancellare?");
                    ID=sc.nextInt();
                    service.delLibro(conn, statement, url, username, password, ID);
                }break;
                case 4:{
                    System.out.println("\nFINE");
                }break;
            }
            if(rispo!=4){
                System.out.println("\nEcco i Libri Presenti nel DB!\n");
                service.getLibro(statement, url, username, password);
            }
        }
        conn.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
}